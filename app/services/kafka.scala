package services

/**
 * Created by oded on 8/7/16.
 */
import java.util.UUID
import javax.inject.{Inject, Singleton}

import akka.kafka.ConsumerSettings
import akka.kafka.scaladsl.Consumer
import akka.stream.Materializer
import akka.stream.scaladsl.Source
import models.KafkaMessageHandler
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import play.api.Configuration

import scala.util.{Failure, Success, Try}

abstract class Kafka {
    def getStreamOfMessages: Try[Source[ConsumerRecord[String, String], Consumer.Control]]
    def handleKafkaMessages(kafka_message_handler:KafkaMessageHandler): String
}

@Singleton
class KafkaImpl @Inject()(configuration: Configuration, materializer: Materializer) extends Kafka {

    val kafka_url = configuration.getString("kafka_url").get
    val kafka_topic = configuration.getString("kafka_topic").get
    val config = configuration.getConfig("akka.kafka.consumer").getOrElse(Configuration.empty)

    def maybeKafkaUrl[K](f: String => K): Try[K] = {
        import java.net.URI
        val kafkaUrls = kafka_url.split(",").map { urlString =>
            val uri = new URI(urlString)
            Seq(uri.getHost, uri.getPort).mkString(":")
        }
        Success(f(kafkaUrls.mkString(",")))

    }

    def consumerSettings: Try[ConsumerSettings[String, String]] = {
        maybeKafkaUrl { kafkaUrl =>
            val deserializer = new StringDeserializer()
            ConsumerSettings[String, String](config.underlying, deserializer, deserializer, Set(kafka_topic))
                .withBootstrapServers(kafkaUrl)
                .withGroupId(UUID.randomUUID().toString)
        }
    }

    def getStreamOfMessages: Try[Source[ConsumerRecord[String, String], Consumer.Control]] = {
        consumerSettings.map(Consumer.plainSource)
    }


    def handleKafkaMessages(kafka_message_handler:KafkaMessageHandler) = {

        getStreamOfMessages match {
            case Failure(e) =>
                "bad"
            case Success(source) =>
                source.runForeach(kafka_message_handler.handleMessage)(materializer)
                "good"
        }

    }


    /*
    consumer for kafka version M4 which does not work
@Singleton
class KafkaImpl @Inject()(configuration: Configuration, materializer: Materializer) extends Kafka {

    val kafka_url = configuration.getString("kafka_url").get
    val kafka_topic = configuration.getString("kafka_topic").get
    val config = configuration.getConfig("akka.kafka.consumer").getOrElse(Configuration.empty)

    def maybeKafkaUrl[K](f: String => K): Try[K] = {
        import java.net.URI
        val kafkaUrls = kafka_url.split(",").map { urlString =>
            val uri = new URI(urlString)
            Seq(uri.getHost, uri.getPort).mkString(":")
        }
        Success(f(kafkaUrls.mkString(",")))

    }

    def consumerSettings: Try[ConsumerSettings[String, String]] = {
        maybeKafkaUrl { kafkaUrl =>
            val deserializer = new StringDeserializer()
            ConsumerSettings[String, String](config.underlying, deserializer, deserializer)
                .withBootstrapServers(kafkaUrl)
                .withGroupId(UUID.randomUUID().toString)
                .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
        }
    }

    def getStreamOfMessages: Try[Source[ConsumerRecord[String, String], Consumer.Control]] = {
        val topic_partition = new TopicPartition(kafka_topic, 1)
//        val subscription = Subscriptions.assignment(topic_partition)
        val subscription = Subscriptions.assignmentWithOffset(topic_partition, 0)

        consumerSettings.map(consumerSettings =>
            Consumer.plainSource(consumerSettings, subscription))
    }

    def handleKafkaMessages(kafka_message_handler:KafkaMessageHandler) = {

        getStreamOfMessages match {
            case Failure(e) =>
                "bad"
            case Success(source) =>
                source.runForeach(kafka_message_handler.handleMessage)(materializer)
                "good"
        }

    }
     */

}