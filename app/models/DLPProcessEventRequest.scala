package models

import play.api.libs.json.{JsObject, Json}

/**
 * Created by oded on 6/27/16.
 */
case class DLPProcessEventRequest(raw_so_id: String, so_owner_id: String, s3_bucket_name: String, callback_url: String, s3_file_name: String, so_id: String) {
    def asJson = DLPProcessEventRequest.asJson(this)
}

object DLPProcessEventRequest {
    implicit val dlp_process_event_request_reader = Json.reads[DLPProcessEventRequest]
    implicit val dlp_process_event_writer = Json.writes[DLPProcessEventRequest]
    def asJson(dlp_process_event_request:DLPProcessEventRequest) = Json.toJson(dlp_process_event_request).asOpt[JsObject]
        .getOrElse(throw new Exception("Bad dlp_process_event_request_reader"))
}
