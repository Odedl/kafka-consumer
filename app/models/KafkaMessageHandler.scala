package models

import javax.inject.{Inject, Singleton}

import org.apache.kafka.clients.consumer.ConsumerRecord
import play.api.Logger
import services.Kafka

/**
 * Created by oded on 8/7/16.
 */

case class KafkaMessageHandlingResult()

trait  KafkaMessageHandler {
    def handleMessage(consumer_record:ConsumerRecord[String, String]):Unit
}

@Singleton
class DLPKafkaMessageHandler extends KafkaMessageHandler{
    def handleMessage(consumer_record:ConsumerRecord[String, String]) :Unit = {
        Logger.debug(s"retrieved the following message from kafka: ${consumer_record.toString}")
    }
}
