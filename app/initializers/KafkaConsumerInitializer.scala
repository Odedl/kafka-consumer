package initializers

import javax.inject.{Inject, Singleton}

import akka.actor.{Actor, ActorSystem, Props}
import models.KafkaMessageHandler
import services.Kafka

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
 * Created by oded on 8/7/16.
 */
@Singleton
class KafkaConsumerInitializer @Inject()(actorSystem: ActorSystem, kafka: Kafka, kafka_message_handler:KafkaMessageHandler) {
    val WARMUP = "WARMUP"

    val reading_messages_actor = actorSystem.actorOf(Props(new Actor {
        def receive = {
            case WARMUP =>
                kafka.handleKafkaMessages(kafka_message_handler)
        }
    }))
        
    actorSystem.scheduler.scheduleOnce(0.seconds, reading_messages_actor, WARMUP)
    
}
